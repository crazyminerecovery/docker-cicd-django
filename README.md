# Руководство по развертыванию блога на Django

В этом репозитории содержится код, который в конечном итоге был бы у пользователя, если бы он прошел [Django Blog tutorial](https://tutorial.djangogirls.org/en/).

[![CircleCI](https://circleci.com/gh/NdagiStanley/django_girls_complete.svg?style=svg)](https://circleci.com/gh/NdagiStanley/django_girls_complete)

## Различия

Выражая свои авторские права, некоторые моменты немного отличаются от вышепредставленного руководства:

- Кнопки `Log in` и `Log out` в заголовке страницы
- A `Back` link within the *blog-detail* and *blog-edit* pages
- Раасширенный `.gitignore` файл
- `.editorconfig` файл
- An additional python package in the requirements.txt: `pycodestyle`

- Внутри `mysite/settings.py`,

  - Использование `Africa/Nairobi` в качестве моей *TIME_ZONE*
  - Использование `en-us` в качестве моего языка *LANGUAGE_CODE*
  - Добавление адреса `0.0.0.0` и `.herokuapp.com` в список разрешенных хостов *ALLOWED_HOSTS* list

## Установка

В виртуальной среде Python запустите:

- `pip install -r requirements.txt`
- `python manage.py migrate blog`
- `python manage.py createsuperuser` (для создания пользователя, которого вы будете использовать для входа в систему)

### Запуск приложения

```bash
python manage.py runserver
```

Теперь все готово. Ваш блог готов.

### Тесты

```bash
python manage.py test
```

Приложение будет доступно в режиме реального времени по адресу [0.0.0.0:8000](0.0.0.0:8000)

### Log in/ out

- Нажмите `Log in` (вы будете перенаправлены в админ-панель)
- На странице администратора введите учетные данные суперпользователя, созданные в [Setup](#setup)
- Нажмите кнопку *Log in* (Вы будете перенаправлены обратно на страницу)
- Нажмите `Log out` чтобы выйти.

### Запись в блоге

- Войдите в систему
- Нажмите на кнопку`+`, введите _**title**_ и _**text**_
- После чего нажмите кнопку `Save`
