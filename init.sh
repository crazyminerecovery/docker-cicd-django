#!/bin/sh

#user=admin
#email=admin@example.com
#password=pass

file="db/db.sqlite3"

if [ ! -f "$file" ]; then
    echo "Файл $file не найден. Выполнение миграций базы данных..."
    python3 manage.py migrate
    echo "Создание суперпользователя Django..."
    echo "from django.contrib.auth.models import User; User.objects.create_superuser('$user', '$email', '$password')" | python3 manage.py shell
else
    echo "Файл $file найден. Выполнение миграций базы данных..."
    python3 manage.py migrate
fi